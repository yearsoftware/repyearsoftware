create database yearsoft;
use yearsoft;

create table categorias(
	tipo varchar(20),
    cor varchar(20),
    primary key (tipo)
)Engine=INNODB;

create table eventos(
	idEvento int PRIMARY KEY AUTO_INCREMENT NOT NULL,
    diaInicio date,
    diaFinal date,
    horaInicio int,
    minutoInicio int,
    horaFinal int,
    minutoFinal int,
    descricao varchar(150),
    importancia int,
    categoria varchar(20),
    foreign key (categoria) references categorias (tipo)
)Engine=INNODB;

create table usuarios(
	nome varchar(50),
	login varchar(30), 
	senha varchar(30),
    telefone varchar(17),
    nascimento date,
    primary key (login)
)ENGINE = INNODB;