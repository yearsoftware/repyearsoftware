package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;

import model.Usuario;

public class UsuarioDAO {
    
    private ModeloMySql modelo =null;
	
	public UsuarioDAO(){
		
		modelo = new ModeloMySql();
		
		modelo.setDatabase("yearsoft");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("");
		modelo.setPort("3306");
		
	}
        
        public boolean Inserir(Usuario user){
                boolean status= true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}
		
		String sql = "INSERT INTO usuarios (nome, login, senha, telefone, nascimento)"
                            + "VALUES(?,?,?,?,?);";
		
		try {
                    PreparedStatement statement = con.prepareStatement(sql);
			
                    statement.setString(1, user.getNome());
                    statement.setString(2, user.getLogin());
                    statement.setString(3, user.getSenha());
                    statement.setString(4, user.getTelefone());
                    statement.setDate(5, user.getNascimento());
			
                    statement.executeUpdate();
                    status = true;
						
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Esse login de usuário já foi cadastrado. Tente novamente.");
			status = false;
		} finally {
			FabricaDeConexao.closeConnection(con);
		}
		return status;
	}
        
        
        public Usuario Pesquisar(String login){
            
            Connection con = FabricaDeConexao.getConnection(modelo);
            
            Usuario user = new Usuario();
            
            if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
            }
	
            String sql = "SELECT * FROM usuarios WHERE login='"+login+"'";
           
            try {
		PreparedStatement statement = con.prepareStatement(sql);
                ResultSet result = statement.executeQuery();
                
                if(result.next()){
                    user.setNome(result.getString("nome"));
                    user.setLogin(result.getString("login"));
                    user.setSenha(result.getString("senha"));
                    user.setTelefone(result.getString("telefone"));
                    user.setNascimento(result.getDate("nascimento"));
                }else{
                    user = null;
                }
                
            }catch(SQLException e){
                JOptionPane.showMessageDialog(null, "O login inserido é inexistente. Tente novamente.");
                user = null;
            }finally{
                FabricaDeConexao.closeConnection(con);
            }
            
            return user;
            
        }
}
