package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import java.sql.Date;
import java.text.SimpleDateFormat;
import model.ModeloMySql;

import model.Categoria;
import model.Evento;

public class EventoDAO {
    
    private ModeloMySql modelo =null;
	
	public EventoDAO(){
		
		modelo = new ModeloMySql();
		
		modelo.setDatabase("yearsoft");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("");
		modelo.setPort("3306");
		
	}
        
        public boolean inserir(Evento evento){
                boolean status= true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}
		
		String sql = "INSERT INTO eventos (diaInicio, diaFinal, horaInicio, minutoInicio, horaFinal, minutoFinal, descricao, importancia, categoria, loginUsuario)"
                            + "VALUES(?,?,?,?,?,?,?,?,?,?);";
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			
                        statement.setDate(1, evento.getDiaInicial());
                        statement.setDate(2, evento.getDiaFinal());
			statement.setInt(3, evento.getHoraInicio());
                        statement.setInt(4, evento.getMinutoInicio());
                        statement.setInt(5, evento.getHoraFinal());
                        statement.setInt(6, evento.getMinutoFinal());
                        statement.setString(7, evento.getDescricao());
                        statement.setInt(8, evento.getImportancia());
                        statement.setString(9, evento.getCategoria());
                        statement.setString(10, evento.getLoginUsuario());
			
			statement.executeUpdate();
                        status = true;
						
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Ocorreu um problema com o cadastro. Tente novamente.");
			status = false;
		} finally {
			FabricaDeConexao.closeConnection(con);
		}
		return status;
	}
        
        public ArrayList <Evento> listar(ArrayList <Evento> eventos, Date data, String login){
            
            Connection con = FabricaDeConexao.getConnection(modelo);            

            if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
            }
	
            String sql = "SELECT * FROM eventos INNER JOIN categorias ON eventos.categoria = categorias.tipo"
                    + " WHERE eventos.diaInicio='"+ data +"' AND eventos.loginUsuario='"+ login +"';";

            try {
		PreparedStatement statement = con.prepareStatement(sql);
                ResultSet result = statement.executeQuery();
                Evento e;
                
                while (result.next()) {
                    e = new Evento();
                    e.setIdEvento(result.getInt("idEvento"));
                    e.setDiaInicial(data);
                    e.setDiaFinal(result.getDate("diaFinal"));
                    e.setHoraInicio(result.getInt("horaInicio"));
                    e.setMinutoInicio(result.getInt("minutoInicio"));
                    e.setHoraFinal(result.getInt("horaFinal"));
                    e.setMinutoFinal(result.getInt("minutoFinal"));
                    e.setDescricao(result.getString("descricao"));
                    e.setCategoria(result.getString("categoria"));
                    e.setImportancia(result.getInt("importancia"));
                    e.setCor(result.getString("cor"));
                    e.setModelo(result.getInt("modelo"));

                    eventos.add(e);
                }

            } catch (SQLException e) {
		JOptionPane.showMessageDialog(null, "Erro na listagem de Eventos.");
                eventos = null;
            } finally {
		FabricaDeConexao.closeConnection(con);
            }

            return eventos;
        }
        
        public boolean editar(Evento evento){
            boolean retorno;
            Connection con = FabricaDeConexao.getConnection(modelo);            

            if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
            }
	
            String sql = "UPDATE eventos SET descricao='"+evento.getDescricao()+"', diaInicio='"+evento.getDiaInicial()+"',"
                    + "diaFinal='"+evento.getDiaFinal()+"', horaInicio="+evento.getHoraInicio()+", minutoInicio="+evento.getMinutoInicio()+","
                    + "horaFinal="+evento.getHoraFinal()+", minutoFinal="+evento.getMinutoFinal()+" WHERE idEvento="+evento.getIdEvento()+";";
            System.out.println("sql = "+ sql);
            
            try {
		PreparedStatement statement = con.prepareStatement(sql);
                statement.executeQuery();
                // ERROOOOO ELE NÃO QUER EXECUTAR O COMANDO E SALVAR OS DADOS ALTERADOSSSSSSSSSSSSSSSSSSS
                retorno = true;
            } catch (SQLException e) {
		JOptionPane.showMessageDialog(null, "Não foi possível realizar a edição dos dados.");
                retorno = false;
            } finally {
		FabricaDeConexao.closeConnection(con);
            }
            
            return retorno;
        }
    
}
