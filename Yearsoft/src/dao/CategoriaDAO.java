package dao;
// comentario teste do git!!!!!
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Categoria;

import controller.FabricaDeConexao;
import model.ModeloMySql;

import model.Categoria;
import model.Usuario;

public class CategoriaDAO {
    
    private ModeloMySql modelo =null;
	
	public CategoriaDAO(){
		
		modelo = new ModeloMySql();
		
		modelo.setDatabase("yearsoft");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("");
		modelo.setPort("3306");
		
	}
        
        public boolean inserir(Categoria categoria){
                boolean status= true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}
		
		String sql = "INSERT INTO categorias (tipo, cor, modelo, loginUsuario)"
                            + "VALUES(?,?,?,?);";
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			
                        statement.setString(1, categoria.getTipo());
			statement.setString(2, categoria.getCor());
                        statement.setInt(3, categoria.getModelo());
                        statement.setString(4, categoria.getLoginUsuario());
                        
			statement.executeUpdate();
                        status = true;
						
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Categoria já cadastrada. Tente novamente.");
			status = false;
		} finally {
			FabricaDeConexao.closeConnection(con);
		}
		return status;
	}
        
        public Categoria Pesquisar(String tipo){

            Connection con = FabricaDeConexao.getConnection(modelo);
            
            Categoria categoria = new Categoria();
            
            if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
            }
	
            String sql = "SELECT * FROM categorias WHERE tipo='"+tipo+"'";
           
            try {
		PreparedStatement statement = con.prepareStatement(sql);
                ResultSet result = statement.executeQuery();
                
                if(result.next()){
                    categoria.setTipo(tipo);
                    categoria.setCor(result.getString("cor"));
                    categoria.setModelo(0);
                }else{
                    System.out.println("Categoria vazia/inexistente");
                    categoria = null;
                }
                
            }catch(SQLException e){
                JOptionPane.showMessageDialog(null, "Não foi encontada categoria do tipo "+tipo);
                categoria = null;
            }finally{
                FabricaDeConexao.closeConnection(con);
            }
            
            return categoria;
            
        }
        
        public ArrayList<Categoria> listar(ArrayList<Categoria> categorias, String login){
            Connection con = FabricaDeConexao.getConnection(modelo);
            
            if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
            }
	
            String sql = "SELECT * FROM categorias WHERE loginUsuario='"+ login +"';";
           
            try {
		PreparedStatement statement = con.prepareStatement(sql);
                ResultSet result = statement.executeQuery();
                Categoria c = new Categoria();
                
                while (result.next()) {
                    c = new Categoria();
                    c.setTipo(result.getString("tipo"));
                    c.setCor(result.getString("cor"));
                    
                    if(!c.getTipo().equals("Padrão")) // Não pesquisa o valor Padrão, já que o mesmo é implementado estaticamente (Sempre existirá o padrão)
                        categorias.add(c); 
                }

            } catch (SQLException e) {
		JOptionPane.showMessageDialog(null, "Erro na listagem de Categorias.");
                categorias = null;
            } finally {
		FabricaDeConexao.closeConnection(con);
            }
            
            return categorias;
        }
    
}
