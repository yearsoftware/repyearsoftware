/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.EventoDAO;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Evento;

public class EventoCTRL {
    
    public boolean cadastrar(Date diaInicio, Date diaFinal, int[] horaInicio, int[] horaFinal, String descricao, int importancia, String categoria, String login){
        Evento evento = new Evento();
        
        evento.setDiaInicial(diaInicio);
        evento.setDiaFinal(diaFinal);
        evento.setHoraInicio(horaInicio[0]);
        evento.setMinutoInicio(horaInicio[1]);
        evento.setHoraFinal(horaFinal[0]);
        evento.setMinutoFinal(horaFinal[1]);
        evento.setDescricao(descricao);
        evento.setImportancia(importancia);
        evento.setCategoria(categoria);
        evento.setLoginUsuario(login);
        
        EventoDAO dao = new EventoDAO();
        
        if(dao.inserir(evento)){
            JOptionPane.showMessageDialog(null, "Evento cadastrado com Sucesso.", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }
        return false;  
    }
    
    public ArrayList <Evento> listar(ArrayList <Evento> eventos, int dia, int mes, int ano, String login) throws ParseException{
        
        EventoDAO dao = new EventoDAO();
        
        String dataSelecionada = ano +"-"+ mes +"-"+ dia; // Determina quando foi o 1º dia do mês
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.sql.Date data = new java.sql.Date(format.parse(dataSelecionada).getTime());
        
        eventos = dao.listar(eventos, data, login);
        
        return eventos;
    }
    
    public boolean editar(Date diaInicio, Date diaFinal, int[] horaInicio, int[] horaFinal, String descricao, int importancia, String categoria, int idEvento){
        
        Evento evento = new Evento();
        
        evento.setDiaInicial(diaInicio);
        evento.setDiaFinal(diaFinal);
        evento.setHoraInicio(horaInicio[0]);
        evento.setMinutoInicio(horaInicio[1]);
        evento.setHoraFinal(horaFinal[0]);
        evento.setMinutoFinal(horaFinal[1]);
        evento.setDescricao(descricao);
        evento.setImportancia(importancia);
        evento.setCategoria(categoria);
        evento.setIdEvento(idEvento);
        
        EventoDAO dao = new EventoDAO();
        
        if(dao.editar(evento)){
            return true;
        }
        return false;
        
    }
    
}
