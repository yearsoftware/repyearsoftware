/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.UsuarioDAO;
import java.sql.Date;
import javax.swing.JOptionPane;
import model.Usuario;

/**
 *
 * @author natan
 */
public class UsuarioCTRL {
    
    public boolean cadastrar(String nome, String login, String senha, String telefone, Date nascimento){
		Usuario user= new Usuario();
		
		user.setNome(nome);
		user.setLogin(login);
		user.setSenha(senha);
		user.setTelefone(telefone);
                user.setTelefone(telefone);
		user.setNascimento(nascimento);
                
                UsuarioDAO dao = new UsuarioDAO();
                
                if(dao.Inserir(user)){
                    JOptionPane.showMessageDialog(null, "Usuário '"+nome+"' cadastrado com Sucesso.", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);
                    return true;
                }
                return false;     
    }
    
    public Usuario pesqusiar(String login){
		Usuario user = new Usuario();
                
                UsuarioDAO dao = new UsuarioDAO();
                user = dao.Pesquisar(login);
                
                return user;
    }
    
}
