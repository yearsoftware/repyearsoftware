/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.CategoriaDAO;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Categoria;
import model.Usuario;

public class CategoriaCTRL {
    
    public boolean cadastrar(String tipo, String cor, int modelo, String login){ 
        Categoria c = new Categoria();
        
        c.setTipo(tipo);
        c.setCor(cor);
        c.setModelo(modelo);
        c.setLoginUsuario(login);
        
        CategoriaDAO dao = new CategoriaDAO();
        
        if(dao.inserir(c)){
            if(!c.getTipo().equals("Padrão"))
                JOptionPane.showMessageDialog(null, "Categoria '"+tipo+"' cadastrada com Sucesso.", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }
        return false;  
    }
    
    public Categoria pesquisar(String tipo){
        Categoria categoria = new Categoria();
        CategoriaDAO dao = new CategoriaDAO();
      
        categoria = dao.Pesquisar(tipo);
        
        return categoria;
    }
    
    public ArrayList <Categoria> listar(ArrayList <Categoria> categorias, Usuario user){
        CategoriaDAO dao = new CategoriaDAO();
      
        categorias = dao.listar(categorias, user.getLogin());
        
        return categorias;
    }
    
}
