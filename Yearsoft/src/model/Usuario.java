package model;

import java.sql.Date;

public class Usuario {
    
    private String nome;
    private String login;
    private String senha;
    private String telefone;
    private Date nascimento;

    public Usuario(){}
    
    public Usuario(String nome, String login, String senha, String telefone, Date nascimento){ // Construtor para tornar tudo mais rápido no cadastro
        this.nome =  nome;
        this.login = login;
        this.senha = senha;
        this.telefone = telefone;
        this.nascimento = nascimento;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }

}
