/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import com.toedter.calendar.JDateChooser;
import config.ComboBoxRenderer;
import controller.CategoriaCTRL;
import controller.EventoCTRL;
import dao.CategoriaDAO;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import model.Categoria;
import model.Evento;
import model.Usuario;

/**
 *
 * @author natan
 */
public class EditarEvento extends javax.swing.JFrame {

    /**
     * Creates new form Editar
     */
    
    private MenuPrincipal classePai;
    private Usuario usuario;
    
    private JTextField txtDescricaoEditar;
    private JDateChooser dateDiaInicioEditar;
    private JDateChooser dateDiaFinalEditar;
    private JFormattedTextField txtHoraInicialEditar;    
    private JFormattedTextField txtHoraFinalEditar;
    
    private Evento evento;
    
    String[] tipos;
    Color[] cores;
    
    public EditarEvento() {
        initComponents();
    }

    public void abrirEdicaoEvento(Evento evento, MenuPrincipal menu, Usuario user){
        this.evento = evento;
        usuario = user;
        classePai = menu;
        this.setVisible(true);
        this.setResizable(false);
        
        String[] diaInicialArray = evento.getDiaInicial().toString().split("-");
        String[] diaFinalArray = evento.getDiaFinal().toString().split("-");
        String diaInicial = diaInicialArray[2] +"/"+ diaInicialArray[1] +"/"+ diaInicialArray[0];
        String diaFinal = diaFinalArray[2] +"/"+ diaFinalArray[1] +"/"+ diaFinalArray[0];
        
        String horaInicial = evento.getHoraInicio() +":"+ evento.getMinutoInicio();
        if(evento.getHoraInicio() < 10 && evento.getMinutoInicio() > 10){
            horaInicial = "0"+ evento.getHoraInicio() +":"+ evento.getMinutoInicio();
        }else if(evento.getHoraInicio() > 10 && evento.getMinutoInicio() < 10){
            horaInicial = evento.getHoraInicio() +":0"+ evento.getMinutoInicio();
        }else if(evento.getHoraInicio() < 10 && evento.getMinutoInicio() < 10){
            horaInicial = "0"+ evento.getHoraInicio() +":0"+ evento.getMinutoInicio();
        }else{
            horaInicial = evento.getHoraInicio() +":"+ evento.getMinutoInicio();
        }

        String horaFinal = evento.getHoraFinal() +":"+ evento.getMinutoFinal();
        if(evento.getHoraFinal() < 10 && evento.getMinutoFinal() > 10){
            horaFinal = "0"+ evento.getHoraFinal() +":"+ evento.getMinutoFinal();
        }else if(evento.getHoraFinal() > 10 && evento.getMinutoFinal() < 10){
            horaFinal = evento.getHoraFinal() +":0"+ evento.getMinutoFinal();
        }else if(evento.getHoraFinal() < 10 && evento.getMinutoFinal() < 10){
            horaFinal = "0"+ evento.getHoraFinal() +":0"+ evento.getMinutoFinal();
        }else{
            horaFinal = evento.getHoraFinal() +":"+ evento.getMinutoFinal();
        }
        
        lblDescricaoEdicaoEvento.setText(evento.getDescricao());
        lblDiaInicialEdicaoEvento.setText(diaInicial);
        lblDiaFinalEdicaoEvento.setText(diaFinal);
        lblHoraInicialEdicaoEvento.setText(horaInicial);
        lblHoraFinalEdicaoEvento.setText(horaFinal);
        importanciaEditar.setImportancia(evento.getImportancia());
        
        /* Preenchimento do comboBox Categorias */
        ArrayList <Categoria> categorias = new ArrayList <Categoria>();
        EventoCTRL controleEvento = new EventoCTRL();
        CategoriaCTRL controleCategoria = new CategoriaCTRL();
        
        Categoria selecione = new Categoria();
        selecione.setTipo("Selecione");
        selecione.setCor("[0,0,0]");
        categorias.add(selecione);
        
        CategoriaDAO dao = new CategoriaDAO();
        if(controleCategoria.pesquisar("Padrão") == null){
            controleCategoria.cadastrar("Padrão", "[0,0,0]", 2, null); // A categoria padrão tem modelo 2 (fundo claro)
        }
        
        Categoria padrao = new Categoria();
        padrao.setTipo("Padrão");
        padrao.setCor("[0,0,0]");
        categorias.add(padrao);
        
        categorias = controleCategoria.listar(categorias, usuario);
        
        tipos = new String[categorias.size()];
        cores = new Color[categorias.size()];
        String cor;
        
        for(int i=0;i<tipos.length;i++){
            tipos[i] = categorias.get(i).getTipo(); // preenchimento do array de tipos (strings)
            cor = categorias.get(i).getCor();
            String[] colors = cor.substring(1,cor.length()-1).split(",");
            Color color = new Color( // Cor convertida
                Integer.parseInt(colors[0].trim()),
                Integer.parseInt(colors[1].trim()),
                Integer.parseInt(colors[2].trim()));
            cores[i] = color; // preenchimento do array de cores
            comboCategoria.addItem(tipos[i]);
        }
        
        ComboBoxRenderer renderer = new ComboBoxRenderer(comboCategoria);

        renderer.setColors(cores);
        renderer.setStrings(tipos);

        comboCategoria.setRenderer(renderer);
        /* Preenchimento do comboBox Categorias */
        comboCategoria.setSelectedItem(evento.getCategoria());
        
        criarEditores();
    }
    
    public void criarEditores(){
        JTextField txtDescricaoEditar = new JTextField();
        txtDescricaoEditar.setLocation(lblDescricaoEdicaoEvento.getLocation());
        txtDescricaoEditar.setSize(lblDescricaoEdicaoEvento.getSize());
        txtDescricaoEditar.setVisible(false);
        txtDescricaoEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescricaoEditarActionPerformed(evt);
            }
        });
        txtDescricaoEditar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDescricaoEditarFocusLost(evt);
            }
        });
        this.txtDescricaoEditar = txtDescricaoEditar;
        
        JDateChooser dateDiaInicioEditar = new JDateChooser();
        dateDiaInicioEditar.setLocation(lblDiaInicialEdicaoEvento.getLocation());
        dateDiaInicioEditar.setSize(lblDiaInicialEdicaoEvento.getSize());
        dateDiaInicioEditar.setVisible(false);
        dateDiaInicioEditar.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dateDiaInicioEditarPropertyChange(evt);
            }
        });
       this.dateDiaInicioEditar = dateDiaInicioEditar;
       
        JDateChooser dateDiaFinalEditar = new JDateChooser();
        dateDiaFinalEditar.setLocation(lblDiaFinalEdicaoEvento.getLocation());
        dateDiaFinalEditar.setSize(lblDiaFinalEdicaoEvento.getSize());
        dateDiaFinalEditar.setVisible(false);
        dateDiaFinalEditar.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dateDiaFinalEditarPropertyChange(evt);
            }
        });
        this.dateDiaFinalEditar = dateDiaFinalEditar;
        
        JFormattedTextField txtHoraInicialEditar = new JFormattedTextField();
        txtHoraInicialEditar.setLocation(lblHoraInicialEdicaoEvento.getLocation());
        txtHoraInicialEditar.setSize(lblHoraInicialEdicaoEvento.getSize());
        try{ txtHoraInicialEditar.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##"))); } catch (ParseException ex) {}
        txtHoraInicialEditar.setVisible(false);
        txtHoraInicialEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHoraInicialEditarActionPerformed(evt);
            }
        });
        txtHoraInicialEditar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtHoraInicialEditarFocusLost(evt);
            }
        });
        this.txtHoraInicialEditar = txtHoraInicialEditar;
        
        JFormattedTextField txtHoraFinalEditar = new JFormattedTextField();
        txtHoraFinalEditar.setLocation(lblHoraFinalEdicaoEvento.getLocation());
        txtHoraFinalEditar.setSize(lblHoraFinalEdicaoEvento.getSize());
        try{ txtHoraFinalEditar.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##"))); } catch (ParseException ex) {}
        txtHoraFinalEditar.setVisible(false);
        txtHoraFinalEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHoraFinalEditarActionPerformed(evt);
            }
        });
        txtHoraFinalEditar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtHoraFinalEditarFocusLost(evt);
            }
        });
        this.txtHoraFinalEditar = txtHoraFinalEditar;
        
        panelEdicaoEvento.add(this.txtDescricaoEditar);
        panelEdicaoEvento.add(this.dateDiaInicioEditar);
        panelEdicaoEvento.add(this.dateDiaFinalEditar);
        panelEdicaoEvento.add(this.txtHoraInicialEditar);
        panelEdicaoEvento.add(this.txtHoraFinalEditar);
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelEdicaoEvento = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblHoraFinalEdicaoEvento = new javax.swing.JLabel();
        lblHoraInicialEdicaoEvento = new javax.swing.JLabel();
        lblDiaFinalEdicaoEvento = new javax.swing.JLabel();
        lblDiaInicialEdicaoEvento = new javax.swing.JLabel();
        btnSalvar = new componentes.Botao1();
        lblDescricaoEdicaoEvento = new javax.swing.JLabel();
        btnEditarDataInicio = new componentes.BotaoEdicao();
        btnEditarDescricao = new componentes.BotaoEdicao();
        btnEditarDataFinal = new componentes.BotaoEdicao();
        btnEditarHoraInicio = new componentes.BotaoEdicao();
        btnEditarHoraFinal = new componentes.BotaoEdicao();
        importanciaEditar = new componentes.Importancia();
        comboCategoria = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(252, 255, 245));

        panelEdicaoEvento.setBackground(new java.awt.Color(252, 255, 245));

        jLabel2.setBackground(new java.awt.Color(25, 52, 65));
        jLabel2.setFont(new java.awt.Font("Open Sans", 0, 36)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(252, 255, 245));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Edição de Evento");
        jLabel2.setOpaque(true);

        jLabel7.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Descrição:");

        jLabel3.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Dia de Início:");

        jLabel4.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Dia de Término:");

        jLabel5.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Hora de Início:");

        jLabel8.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Importância:");

        jLabel6.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Hora de Término:");

        jLabel9.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Categoria:");

        lblHoraFinalEdicaoEvento.setFont(new java.awt.Font("Open Sans", 0, 14)); // NOI18N
        lblHoraFinalEdicaoEvento.setText("<Hora de Término>");

        lblHoraInicialEdicaoEvento.setFont(new java.awt.Font("Open Sans", 0, 14)); // NOI18N
        lblHoraInicialEdicaoEvento.setText("<Hora de Início>");

        lblDiaFinalEdicaoEvento.setFont(new java.awt.Font("Open Sans", 0, 14)); // NOI18N
        lblDiaFinalEdicaoEvento.setText("<Dia de Término>");

        lblDiaInicialEdicaoEvento.setFont(new java.awt.Font("Open Sans", 0, 14)); // NOI18N
        lblDiaInicialEdicaoEvento.setText("<Data de Início>");

        btnSalvar.setText("Salvar");
        btnSalvar.setFont(new java.awt.Font("Open Sans", 0, 16)); // NOI18N
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        lblDescricaoEdicaoEvento.setFont(new java.awt.Font("Open Sans", 0, 14)); // NOI18N
        lblDescricaoEdicaoEvento.setText("<Descrição>");

        btnEditarDataInicio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnEditarDataInicioMouseReleased(evt);
            }
        });

        btnEditarDescricao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnEditarDescricaoMouseReleased(evt);
            }
        });

        btnEditarDataFinal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnEditarDataFinalMouseReleased(evt);
            }
        });

        btnEditarHoraInicio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnEditarHoraInicioMouseReleased(evt);
            }
        });

        btnEditarHoraFinal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnEditarHoraFinalMouseReleased(evt);
            }
        });

        importanciaEditar.setOpaque(false);

        comboCategoria.setFont(new java.awt.Font("Open Sans", 1, 16)); // NOI18N

        javax.swing.GroupLayout panelEdicaoEventoLayout = new javax.swing.GroupLayout(panelEdicaoEvento);
        panelEdicaoEvento.setLayout(panelEdicaoEventoLayout);
        panelEdicaoEventoLayout.setHorizontalGroup(
            panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblHoraFinalEdicaoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnEditarHoraFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblHoraInicialEdicaoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnEditarHoraInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblDescricaoEdicaoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnEditarDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblDiaFinalEdicaoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnEditarDataFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblDiaInicialEdicaoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnEditarDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                        .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(comboCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(importanciaEditar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(10, 10, 10))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelEdicaoEventoLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );
        panelEdicaoEventoLayout.setVerticalGroup(
            panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel7)
                        .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                            .addGap(1, 1, 1)
                            .addComponent(lblDescricaoEdicaoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnEditarDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btnEditarDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblDiaInicialEdicaoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblDiaFinalEdicaoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnEditarDataFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btnEditarHoraInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblHoraInicialEdicaoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblHoraFinalEdicaoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnEditarHoraFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelEdicaoEventoLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelEdicaoEventoLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(importanciaEditar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(panelEdicaoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(comboCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelEdicaoEvento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelEdicaoEvento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        try{
            String[] dataInicio = lblDiaInicialEdicaoEvento.getText().split("/");
            String dataInicioStr = dataInicio[2] +"-"+ dataInicio[1] +"-"+ dataInicio[0];
            String[] dataFinal = lblDiaFinalEdicaoEvento.getText().split("/");
            String dataFinalStr = dataFinal[2] +"-"+ dataFinal[1] +"-"+ dataFinal[0];
            System.out.println("dataInicioStr: "+ dataInicioStr);
            System.out.println("dataFinalStr: "+ dataFinalStr);
            Date dataInicioSql = java.sql.Date.valueOf(dataInicioStr);
            Date dataFinalSql = java.sql.Date.valueOf(dataFinalStr);
            
            String[] Hinicio = lblHoraInicialEdicaoEvento.getText().split(":"); // String auxiliar
            int[] horaInicio = new int[2];
            horaInicio[0] = Integer.parseInt(Hinicio[0]);
            horaInicio[1] = Integer.parseInt(Hinicio[1]);
            String[] Hfinal = lblHoraFinalEdicaoEvento.getText().split(":"); // String auxiliar
            int[] horaFinal = new int[2];
            horaFinal[0] = Integer.parseInt(Hfinal[0]);
            horaFinal[1] = Integer.parseInt(Hfinal[1]);
            String descricao = lblDescricaoEdicaoEvento.getText();
            int importancia = importanciaEditar.getImportancia();
            String categoria = comboCategoria.getSelectedItem().toString();
            
            if(importancia==0 || descricao.equals("") || categoria.equals("Selecione")){
                JOptionPane.showMessageDialog(null, "Preencha todos os dados corretamente antes de submetê-los", "PREENCHIMENTO INCORRETO", JOptionPane.ERROR_MESSAGE);
            }else{
                String[] dataInicioStr2 = dataInicioStr.split("-");
                String[] dataFinalStr2 = dataFinalStr.split("-");
                int[] dataInicioInt = new int[3];
                int[] dataFinalInt = new int[3];
                dataInicioInt[0] = Integer.parseInt(dataInicioStr2[0]); // Ano
                dataInicioInt[1] = Integer.parseInt(dataInicioStr2[1]); // Mês
                dataInicioInt[2] = Integer.parseInt(dataInicioStr2[2]); // Dia
                dataFinalInt[0] = Integer.parseInt(dataFinalStr2[0]); // Ano
                dataFinalInt[1] = Integer.parseInt(dataFinalStr2[1]); // Mês
                dataFinalInt[2] = Integer.parseInt(dataFinalStr2[2]); // Dia
                
                EventoCTRL controleEvento = new EventoCTRL();
                
                if(dataFinalInt[0] < dataInicioInt[0]){ // Ano final antes do ano inicial
                    JOptionPane.showMessageDialog(null, "A data de Término do evento é anterior à data de início. Datas incorretas.", "PREENCHIMENTO INCORRETO", JOptionPane.ERROR_MESSAGE);
                }else if(dataFinalInt[0] == dataInicioInt[0]){ // Mesmo ano
                    if(dataFinalInt[1] < dataInicioInt[1]){ // Mês final antes do mês inicial
                        JOptionPane.showMessageDialog(null, "A data de Término do evento é anterior à data de início. Datas incorretas.", "PREENCHIMENTO INCORRETO", JOptionPane.ERROR_MESSAGE);
                    }else if(dataFinalInt[1] == dataInicioInt[1]){ // Mesmo mês
                        if(dataFinalInt[2] < dataInicioInt[2]){ // Dia final antes do dia inicial
                            JOptionPane.showMessageDialog(null, "A data de Término do evento é anterior à data de início. Datas incorretas.", "PREENCHIMENTO INCORRETO", JOptionPane.ERROR_MESSAGE);
                        }else if(dataFinalInt[2] == dataInicioInt[2]){ // Mesmo dia -- PERMITE CADASTRO (checar horário, já que é no mesmo dia)
                            if(horaFinal[0] < horaInicio[0]){
                                JOptionPane.showMessageDialog(null, "O horário final é anterior ao horário inicial. Horários incorretos.", "PREENCHIMENTO INCORRETO", JOptionPane.ERROR_MESSAGE);
                            }else if(horaFinal[0] == horaInicio[0]){ // horas iguais -- CHECAR MINUTOS
                                if(horaFinal[1] < horaInicio[1]){ // horas iguais E minuto final < minuto inicial
                                    JOptionPane.showMessageDialog(null, "O horário final é anterior ao horário inicial. Horários incorretos.", "PREENCHIMENTO INCORRETO", JOptionPane.ERROR_MESSAGE);
                                }else{ // horas iguais e minutoFinal >= minutoInicial -- CADASTRO
                                    controleEvento.editar(dataInicioSql, dataFinalSql, horaInicio, horaFinal, descricao, importancia, categoria, evento.getIdEvento()); // EXECUÇÃO DA EDIÇÃO
                                }
                            }else{ // horas diferentes, com a final maior que a incial -- CADASTRO
                                controleEvento.editar(dataInicioSql, dataFinalSql, horaInicio, horaFinal, descricao, importancia, categoria, evento.getIdEvento()); // EXECUÇÃO DA EDIÇÃO
                            }
                            
                        }else{ // Evento que termina em dia diferente -- PERMITE CADASTRO COM CONFIRMAÇÃO
                            int confirmacao = JOptionPane.showConfirmDialog(null,"O evento ocorrerá em um período superior a um DIA. Deseja prosseguir com o cadastro?","Confirmação de Cadastro", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                            if (confirmacao == JOptionPane.YES_OPTION) {
                                controleEvento.editar(dataInicioSql, dataFinalSql, horaInicio, horaFinal, descricao, importancia, categoria, evento.getIdEvento()); // EXECUÇÃO DA EDIÇÃO
                            }
                        }
                    }else{ // Evento que termina em mês diferente -- PERMITE CADASTRO COM CONFIRMAÇÃO
                        int confirmacao = JOptionPane.showConfirmDialog(null,"O evento ocorrerá em um período superior a um MÊS. Deseja prosseguir com o cadastro?","Confirmação de Cadastro", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                        if (confirmacao == JOptionPane.YES_OPTION) {
                            controleEvento.editar(dataInicioSql, dataFinalSql, horaInicio, horaFinal, descricao, importancia, categoria, evento.getIdEvento()); // EXECUÇÃO DA EDIÇÃO
                        }
                    }
                }else{ // Evento que termina em ano diferente -- PERMITE CADASTRO COM CONFIRMAÇÃO
                    int confirmacao = JOptionPane.showConfirmDialog(null,"O evento ocorrerá em um período superior a um ANO. Deseja prosseguir com o cadastro?","Confirmação de Cadastro", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if (confirmacao == JOptionPane.YES_OPTION) {
                        controleEvento.editar(dataInicioSql, dataFinalSql, horaInicio, horaFinal, descricao, importancia, categoria, evento.getIdEvento()); // EXECUÇÃO DA EDIÇÃO
                    }
                }
                
            }  
        }catch(NullPointerException e){
            JOptionPane.showMessageDialog(null, "As datas não foram preenchidas corretamente.", "PREENCHIMENTO INCORRETO", JOptionPane.ERROR_MESSAGE);
        }catch(NumberFormatException e){
            JOptionPane.showMessageDialog(null, "As horas não foram preenchidas corretamente.", "PREENCHIMENTO INCORRETO", JOptionPane.ERROR_MESSAGE); 
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnEditarDescricaoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditarDescricaoMouseReleased
        if(btnEditarDescricao.getPressed()){
            lblDescricaoEdicaoEvento.setVisible(false);
            txtDescricaoEditar.setText(lblDescricaoEdicaoEvento.getText());
            txtDescricaoEditar.setVisible(true);
        }
    }//GEN-LAST:event_btnEditarDescricaoMouseReleased

    private void txtDescricaoEditarActionPerformed(java.awt.event.ActionEvent evt) {  
        txtDescricaoEditar.setVisible(false);        
        if(!txtDescricaoEditar.getText().equals(""))
            lblDescricaoEdicaoEvento.setText(txtDescricaoEditar.getText());
        lblDescricaoEdicaoEvento.setVisible(true);
        panelEdicaoEvento.repaint();
    }
    
    private void txtDescricaoEditarFocusLost(java.awt.event.FocusEvent evt) {  
        txtDescricaoEditar.setVisible(false);
        if(!txtDescricaoEditar.getText().equals(""))
            lblDescricaoEdicaoEvento.setText(txtDescricaoEditar.getText());
        lblDescricaoEdicaoEvento.setVisible(true);
        panelEdicaoEvento.repaint();
    }
    
    boolean seguro = true;
    
    private void btnEditarDataInicioMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditarDataInicioMouseReleased
        if(btnEditarDataInicio.getPressed()){
            lblDiaInicialEdicaoEvento.setVisible(false);
            dateDiaInicioEditar.setVisible(true);
            seguro = true;
        }
    }//GEN-LAST:event_btnEditarDataInicioMouseReleased
    
    private void dateDiaInicioEditarPropertyChange(java.beans.PropertyChangeEvent evt) {                                              
        if(dateDiaInicioEditar.getDate() != null){
            if(seguro){
                seguro = false;
                String dataSelecionada = new SimpleDateFormat("dd/MM/yyyy").format(dateDiaInicioEditar.getDate());
                dateDiaInicioEditar.setVisible(false);
                lblDiaInicialEdicaoEvento.setText(dataSelecionada);
                lblDiaInicialEdicaoEvento.setVisible(true);
                panelEdicaoEvento.repaint();
            }else{
                seguro = true;   
            }
        }
    }
    
    private void btnEditarDataFinalMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditarDataFinalMouseReleased
        if(btnEditarDataFinal.getPressed()){
            lblDiaFinalEdicaoEvento.setVisible(false);
            dateDiaFinalEditar.setVisible(true);
            seguro = true;
        }
    }//GEN-LAST:event_btnEditarDataFinalMouseReleased

    private void dateDiaFinalEditarPropertyChange(java.beans.PropertyChangeEvent evt) {                                              
        if(dateDiaFinalEditar.getDate() != null){
            if(seguro){
                seguro = false;
                String dataSelecionada = new SimpleDateFormat("dd/MM/yyyy").format(dateDiaFinalEditar.getDate());
                dateDiaFinalEditar.setVisible(false);
                lblDiaFinalEdicaoEvento.setText(dataSelecionada);
                lblDiaFinalEdicaoEvento.setVisible(true);
                panelEdicaoEvento.repaint();
            }else{
                seguro = true;   
            }
        }
    }
    
    private void btnEditarHoraInicioMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditarHoraInicioMouseReleased
        if(btnEditarHoraInicio.getPressed()){
            lblHoraInicialEdicaoEvento.setVisible(false);
            txtHoraInicialEditar.setVisible(true);
        }
    }//GEN-LAST:event_btnEditarHoraInicioMouseReleased

    private void txtHoraInicialEditarActionPerformed(java.awt.event.ActionEvent evt) {  
        txtHoraInicialEditar.setVisible(false);        
        if(!txtHoraInicialEditar.getText().equals(""))
            lblHoraInicialEdicaoEvento.setText(txtHoraInicialEditar.getText());
        lblHoraInicialEdicaoEvento.setVisible(true);
        panelEdicaoEvento.repaint();
    }
    
    private void txtHoraInicialEditarFocusLost(java.awt.event.FocusEvent evt) {  
        txtHoraInicialEditar.setVisible(false);        
        if(!txtHoraInicialEditar.getText().equals(""))
            lblHoraInicialEdicaoEvento.setText(txtHoraInicialEditar.getText());
        lblHoraInicialEdicaoEvento.setVisible(true);
        panelEdicaoEvento.repaint();
    }
    
    private void btnEditarHoraFinalMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditarHoraFinalMouseReleased
        if(btnEditarHoraFinal.getPressed()){
            lblHoraFinalEdicaoEvento.setVisible(false);
            txtHoraFinalEditar.setVisible(true);
        }
    }//GEN-LAST:event_btnEditarHoraFinalMouseReleased

    private void txtHoraFinalEditarActionPerformed(java.awt.event.ActionEvent evt) {  
        txtHoraFinalEditar.setVisible(false);        
        if(!txtHoraFinalEditar.getText().equals(""))
            lblHoraFinalEdicaoEvento.setText(txtHoraFinalEditar.getText());
        lblHoraFinalEdicaoEvento.setVisible(true);
        panelEdicaoEvento.repaint();
    }
    
    private void txtHoraFinalEditarFocusLost(java.awt.event.FocusEvent evt) {  
        txtHoraFinalEditar.setVisible(false);        
        if(!txtHoraFinalEditar.getText().equals(""))
            lblHoraFinalEdicaoEvento.setText(txtHoraFinalEditar.getText());
        lblHoraFinalEdicaoEvento.setVisible(true);
        panelEdicaoEvento.repaint();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EditarEvento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EditarEvento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EditarEvento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EditarEvento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditarEvento().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private componentes.BotaoEdicao btnEditarDataFinal;
    private componentes.BotaoEdicao btnEditarDataInicio;
    private componentes.BotaoEdicao btnEditarDescricao;
    private componentes.BotaoEdicao btnEditarHoraFinal;
    private componentes.BotaoEdicao btnEditarHoraInicio;
    private componentes.Botao1 btnSalvar;
    private javax.swing.JComboBox<String> comboCategoria;
    private componentes.Importancia importanciaEditar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel lblDescricaoEdicaoEvento;
    private javax.swing.JLabel lblDiaFinalEdicaoEvento;
    private javax.swing.JLabel lblDiaInicialEdicaoEvento;
    private javax.swing.JLabel lblHoraFinalEdicaoEvento;
    private javax.swing.JLabel lblHoraInicialEdicaoEvento;
    private javax.swing.JPanel panelEdicaoEvento;
    // End of variables declaration//GEN-END:variables
}
