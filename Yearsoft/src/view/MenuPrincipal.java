/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import componentes.DiaCalendario;
import componentes.EventoExibicaoCalendario;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import model.Evento;
import model.Usuario;

/**
 *
 * @author natan
 */
public class MenuPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form MenuPrincipal
     */
    
    private Usuario usuario;
    
    /* Calendário */
    private int dia;
    private int mes;
    private int ano;
    private int[] diasPreenchidos = new int[42];
    /* Calendário */
    
    public MenuPrincipal() {
        initComponents();
    }

    /* Menu Principal */
        /** Inicia o menu a partir do login. Define o usuário atualmente online */
        public void abrirMenu(Usuario user){
            this.setVisible(true);
            usuario = user;
            btnPerfil.setText(user.getNome());
            
            inicializarCalendario();
        }

        /** Atualiza o status dos botões de acordo com os valores estabelecidos nos parâmetros. Altera os botões selecionados */
        public void switchButtons(boolean home, boolean categorias, boolean eventos, boolean grupos, boolean calendario){
            btnHome.setSelected(home);
            btnMenuCategorias.setSelected(categorias);
            btnMenuEventos.setSelected(eventos);
            btnMenuGrupos.setSelected(grupos);
            btnMenuCalendario.setSelected(calendario);
        }
    /* Menu Principal */
    
    
    /* Calendário */
        
        private Evento eventoSelecionado;
        
        public void limparDadosEvento(){
            lblDescricaoEvento.setText("");
            lblCategoriaListagemEvento.setText("");
            lblHoraInicioListagemEvento.setText("");
            lblHoraFinalListagemEvento.setText("");
            importanciaListagemEvento.setImportancia(0);
        }
        
        /** Recebe o dia em que o usuário clicou e exibe os dados do mesmo na janela ao lado do calendário */
        public void receberEventoDia(int dia, ArrayList <Evento> eventos){
            lblDiaDestaque.setText(Integer.toString(dia));
            System.out.println("Eventos.size = "+ eventos.size());
            panelDiaDestaque.removeAll();
            panelDiaDestaque.repaint();
            
            int posY = 0;
            
            for(int i=0;i<eventos.size();i++){
                EventoExibicaoCalendario evento = new EventoExibicaoCalendario();
                
                evento.setEvento(eventos.get(i), this); // Envia o evento e a própria classe, para que o mesmo possa retornar a informação
                evento.setSize(389, 25);
                evento.setHorizontalAlignment(JLabel.CENTER);
                evento.setFont(new Font("Open Sans", Font.PLAIN, 20));
                evento.setText(eventos.get(i).getDescricao());
                evento.setLocation(0, posY);
                posY += 27;
                
                // Define o background do evento para ser igual a cor escolhida pelo usuário
                String[] cor = eventos.get(i).getCor().substring(1,eventos.get(i).getCor().length()-1).split(",");
                evento.setBackground(new Color(Integer.parseInt(cor[0]),Integer.parseInt(cor[1]),Integer.parseInt(cor[2])));
                evento.setOpaque(true);
                // Define o background do evento para ser igual a cor escolhida pelo usuário
                
                switch(eventos.get(i).getModelo()){
                    case 1:
                        evento.setForeground(new Color(25,52,65));
                        break;
                    case 2:
                        evento.setForeground(new Color(209,219,189));
                        break;    
                    default:
                        evento.setForeground(Color.black);
                }
                evento.setVisible(true);
                
                evento.setText(eventos.get(i).getDescricao());
                
                panelDiaDestaque.add(evento);
            }
            panelDiaDestaque.repaint();
        }
        
        /** Lista todos os dados do evento selecionado pelo usuário */
        public void receberDadosEvento(Evento evento){
            eventoSelecionado = evento;
            lblDescricaoEvento.setText(evento.getDescricao());
            lblCategoriaListagemEvento.setText(evento.getCategoria());
            String horaInicial = evento.getHoraInicio() +":"+ evento.getMinutoInicio();
            if(evento.getHoraInicio() < 10 && evento.getMinutoInicio() > 10){
                horaInicial = "0"+ evento.getHoraInicio() +":"+ evento.getMinutoInicio();
            }else if(evento.getHoraInicio() > 10 && evento.getMinutoInicio() < 10){
                horaInicial = evento.getHoraInicio() +":0"+ evento.getMinutoInicio();
            }else if(evento.getHoraInicio() < 10 && evento.getMinutoInicio() < 10){
                horaInicial = "0"+ evento.getHoraInicio() +":0"+ evento.getMinutoInicio();
            }else{
                horaInicial = evento.getHoraInicio() +":"+ evento.getMinutoInicio();
            }
            
            String horaFinal = evento.getHoraFinal() +":"+ evento.getMinutoFinal();
            if(evento.getHoraFinal() < 10 && evento.getMinutoFinal() > 10){
                horaFinal = "0"+ evento.getHoraFinal() +":"+ evento.getMinutoFinal();
            }else if(evento.getHoraFinal() > 10 && evento.getMinutoFinal() < 10){
                horaFinal = evento.getHoraFinal() +":0"+ evento.getMinutoFinal();
            }else if(evento.getHoraFinal() < 10 && evento.getMinutoFinal() < 10){
                horaFinal = "0"+ evento.getHoraFinal() +":0"+ evento.getMinutoFinal();
            }else{
                horaFinal = evento.getHoraFinal() +":"+ evento.getMinutoFinal();
            }

            lblHoraInicioListagemEvento.setText(horaInicial);
            lblHoraFinalListagemEvento.setText(horaFinal);
            importanciaListagemEvento.setImportancia(evento.getImportancia());
        }
        
        /** Atualiza as datas do calendário para a data atual, consequentemente atualizando os dias do calendário */
        public void inicializarCalendario(){
            java.util.Date now = new java.util.Date();
            String dataAtualStr = new SimpleDateFormat("yyyy-MM-dd").format(now);
            String[] dataAtualStr2 = dataAtualStr.split("-");
            this.ano = Integer.parseInt(dataAtualStr2[0]);
            this.mes = Integer.parseInt(dataAtualStr2[1]);
            this.dia = Integer.parseInt(dataAtualStr2[2]);
            spinAno.setModel(new SpinnerNumberModel(this.ano, this.ano-50, this.ano+50, 1));
            spinAno.setEditor(new JSpinner.NumberEditor(spinAno, "#"));
            
            // Envia qual a classe na qual os dias estão inseridos
            dia1.setClassePai(this, usuario);
            dia2.setClassePai(this, usuario);
            dia3.setClassePai(this, usuario);
            dia4.setClassePai(this, usuario);
            dia5.setClassePai(this, usuario);
            dia6.setClassePai(this, usuario);
            dia7.setClassePai(this, usuario);
            dia8.setClassePai(this, usuario);
            dia9.setClassePai(this, usuario);
            dia10.setClassePai(this, usuario);
            dia11.setClassePai(this, usuario);
            dia12.setClassePai(this, usuario);
            dia13.setClassePai(this, usuario);;
            dia14.setClassePai(this, usuario);
            dia15.setClassePai(this, usuario);
            dia16.setClassePai(this, usuario);
            dia17.setClassePai(this, usuario);
            dia18.setClassePai(this, usuario);
            dia19.setClassePai(this, usuario);
            dia20.setClassePai(this, usuario);
            dia21.setClassePai(this, usuario);
            dia22.setClassePai(this, usuario);
            dia23.setClassePai(this, usuario);
            dia24.setClassePai(this, usuario);
            dia25.setClassePai(this, usuario);
            dia26.setClassePai(this, usuario);
            dia27.setClassePai(this, usuario);
            dia28.setClassePai(this, usuario);
            dia29.setClassePai(this, usuario);
            dia30.setClassePai(this, usuario);
            dia31.setClassePai(this, usuario);
            dia32.setClassePai(this, usuario);
            dia33.setClassePai(this, usuario);
            dia34.setClassePai(this, usuario);
            dia35.setClassePai(this, usuario);
            dia36.setClassePai(this, usuario);
            dia37.setClassePai(this, usuario);
            dia38.setClassePai(this, usuario);
            dia39.setClassePai(this, usuario);
            dia40.setClassePai(this, usuario);
            dia41.setClassePai(this, usuario);
            dia42.setClassePai(this, usuario);
            // Envia qual a classe na qual os dias estão inseridos
            
            atualizarCalendario(); // Atualiza os dados e os dias do mês atual, assim como os eventos do mesmo
        }

        /** Preenche a numeração no calendário */
        public void enumerarDias(int numeroDiasMes) throws ParseException{
            int aux=1;
            
            for(int i=0;i<42;i++){
                // Necessário zerar o array, para próximas mudanças da data
                diasPreenchidos[i] = 0;
            }
            
            String dataSelecionada = this.ano +"-"+ this.mes +"-"+ 1; // Determina quando foi o 1º dia do mês
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date data = new java.sql.Date(format.parse(dataSelecionada).getTime());
            
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(data);
            int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
            aux = 1;
            switch(diaSemana){
                case 1: // Mês começa no DOMINGO
                    for(int i=0;i<numeroDiasMes;i++){
                        diasPreenchidos[i] = aux;
                        aux++;
                    }
                    break;
                case 2: // Mês começa na SEGUNDA
                    for(int i=1;i<numeroDiasMes+1;i++){
                        diasPreenchidos[i] = aux;
                        aux++;
                    }
                    break;
                case 3: // Mês começa na TERÇA
                    for(int i=2;i<numeroDiasMes+2;i++){
                        diasPreenchidos[i] = aux;
                        aux++;
                    }
                    break;
                case 4: // Mês começa na QUARTA
                    for(int i=3;i<numeroDiasMes+3;i++){
                        diasPreenchidos[i] = aux;
                        aux++;
                    }
                    break;
                case 5: // Mês começa na QUINTA
                    for(int i=4;i<numeroDiasMes+4;i++){
                        diasPreenchidos[i] = aux;
                        aux++;
                    }
                    break;
                case 6: // Mês começa na SEXTA
                    for(int i=5;i<numeroDiasMes+5;i++){
                        diasPreenchidos[i] = aux;
                        aux++;
                    }
                    break;
                case 7: // Mês começa no SÁBADO
                    for(int i=6;i<numeroDiasMes+6;i++){
                        diasPreenchidos[i] = aux;
                        aux++;
                    }
                    break;
            }
            try{
                dia1.setDia(diasPreenchidos[0], this.mes, this.ano);
                dia2.setDia(diasPreenchidos[1], this.mes, this.ano);
                dia3.setDia(diasPreenchidos[2], this.mes, this.ano);
                dia4.setDia(diasPreenchidos[3], this.mes, this.ano);
                dia5.setDia(diasPreenchidos[4], this.mes, this.ano);
                dia6.setDia(diasPreenchidos[5], this.mes, this.ano);
                dia7.setDia(diasPreenchidos[6], this.mes, this.ano);
                dia8.setDia(diasPreenchidos[7], this.mes, this.ano);
                dia9.setDia(diasPreenchidos[8], this.mes, this.ano);
                dia10.setDia(diasPreenchidos[9], this.mes, this.ano);
                dia11.setDia(diasPreenchidos[10], this.mes, this.ano);
                dia12.setDia(diasPreenchidos[11], this.mes, this.ano);
                dia13.setDia(diasPreenchidos[12], this.mes, this.ano);
                dia14.setDia(diasPreenchidos[13], this.mes, this.ano);
                dia15.setDia(diasPreenchidos[14], this.mes, this.ano);
                dia16.setDia(diasPreenchidos[15], this.mes, this.ano);
                dia17.setDia(diasPreenchidos[16], this.mes, this.ano);
                dia18.setDia(diasPreenchidos[17], this.mes, this.ano);
                dia19.setDia(diasPreenchidos[18], this.mes, this.ano);
                dia20.setDia(diasPreenchidos[19], this.mes, this.ano);
                dia21.setDia(diasPreenchidos[20], this.mes, this.ano);
                dia22.setDia(diasPreenchidos[21], this.mes, this.ano);
                dia23.setDia(diasPreenchidos[22], this.mes, this.ano);
                dia24.setDia(diasPreenchidos[23], this.mes, this.ano);
                dia25.setDia(diasPreenchidos[24], this.mes, this.ano);
                dia26.setDia(diasPreenchidos[25], this.mes, this.ano);
                dia27.setDia(diasPreenchidos[26], this.mes, this.ano);
                dia28.setDia(diasPreenchidos[27], this.mes, this.ano);
                dia29.setDia(diasPreenchidos[28], this.mes, this.ano);
                dia30.setDia(diasPreenchidos[29], this.mes, this.ano);
                dia31.setDia(diasPreenchidos[30], this.mes, this.ano);
                dia32.setDia(diasPreenchidos[31], this.mes, this.ano);
                dia33.setDia(diasPreenchidos[32], this.mes, this.ano);
                dia34.setDia(diasPreenchidos[33], this.mes, this.ano);
                dia35.setDia(diasPreenchidos[34], this.mes, this.ano);
                dia36.setDia(diasPreenchidos[35], this.mes, this.ano);
                dia37.setDia(diasPreenchidos[36], this.mes, this.ano);
                dia38.setDia(diasPreenchidos[37], this.mes, this.ano);
                dia39.setDia(diasPreenchidos[38], this.mes, this.ano);
                dia40.setDia(diasPreenchidos[39], this.mes, this.ano);
                dia41.setDia(diasPreenchidos[40], this.mes, this.ano);
                dia42.setDia(diasPreenchidos[41], this.mes, this.ano);
            } catch (ParseException e){
                JOptionPane.showMessageDialog(null, "Houve algum problema na definição dos dias do mês selecionado", "TRETA", JOptionPane.ERROR_MESSAGE);
            }
            
        }
        
        /** A partir da data atual estabelecida no calendário, o calendário será atualizado */
        public void atualizarCalendario(){
            int numeroDiasMes; // Número de dias que o mês terá
            
            comboMes.setSelectedIndex(this.mes-1);
            visorMes.setText(comboMes.getSelectedItem().toString());
            visorAno.setText(Integer.toString(this.ano));
            lblDataMarcada.setText(comboMes.getSelectedItem() +" de "+ Integer.toString(this.ano));
            
            if(comboMes.getSelectedIndex() == 1){ // FEVEREIRO
                if(this.ano%4 == 0){ // ANO BISSEXTO
                    numeroDiasMes = 29;
                }else{
                    numeroDiasMes = 28;
                }
            }else if(comboMes.getSelectedIndex() == 3 || comboMes.getSelectedIndex() == 5 ||
                     comboMes.getSelectedIndex() == 8 || comboMes.getSelectedIndex() == 10){
                numeroDiasMes = 30;
            }else{
                numeroDiasMes = 31;
            }
            
            try{
                enumerarDias(numeroDiasMes);
            }catch(ParseException e){
                JOptionPane.showMessageDialog(null, "Não foi possível determinar a configuração do calendário nessa data.", "DATA SELECIONADA NÃO LOCALIZADA", JOptionPane.ERROR_MESSAGE);
            }
            
            // ENUMERAÇÃO DOS DIAS
            
            
        }
    /* Calendário */
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MenuPrincipal = new javax.swing.JPanel();
        BarraFeramentas = new javax.swing.JPanel();
        btnPerfil = new javax.swing.JLabel();
        btnMenuCategorias = new componentes.Botao2();
        btnMenuEventos = new componentes.Botao2();
        btnMenuGrupos = new componentes.Botao2();
        btnMenuCalendario = new componentes.Botao2();
        btnHome = new componentes.Botao2();
        CardMenu = new javax.swing.JPanel();
        MenuEventos = new javax.swing.JPanel();
        btnCadastrarEvento = new componentes.Botao1();
        MenuGrupos = new javax.swing.JPanel();
        Perfil = new javax.swing.JPanel();
        Calendario = new javax.swing.JPanel();
        calendario = new javax.swing.JPanel();
        dia1 = new componentes.DiaCalendario();
        dia2 = new componentes.DiaCalendario();
        dia3 = new componentes.DiaCalendario();
        dia4 = new componentes.DiaCalendario();
        dia5 = new componentes.DiaCalendario();
        dia6 = new componentes.DiaCalendario();
        dia7 = new componentes.DiaCalendario();
        dia8 = new componentes.DiaCalendario();
        dia9 = new componentes.DiaCalendario();
        dia10 = new componentes.DiaCalendario();
        dia11 = new componentes.DiaCalendario();
        dia12 = new componentes.DiaCalendario();
        dia13 = new componentes.DiaCalendario();
        dia14 = new componentes.DiaCalendario();
        dia15 = new componentes.DiaCalendario();
        dia16 = new componentes.DiaCalendario();
        dia17 = new componentes.DiaCalendario();
        dia18 = new componentes.DiaCalendario();
        dia19 = new componentes.DiaCalendario();
        dia20 = new componentes.DiaCalendario();
        dia21 = new componentes.DiaCalendario();
        dia22 = new componentes.DiaCalendario();
        dia23 = new componentes.DiaCalendario();
        dia24 = new componentes.DiaCalendario();
        dia25 = new componentes.DiaCalendario();
        dia26 = new componentes.DiaCalendario();
        dia27 = new componentes.DiaCalendario();
        dia28 = new componentes.DiaCalendario();
        dia29 = new componentes.DiaCalendario();
        dia30 = new componentes.DiaCalendario();
        dia31 = new componentes.DiaCalendario();
        dia32 = new componentes.DiaCalendario();
        dia33 = new componentes.DiaCalendario();
        dia34 = new componentes.DiaCalendario();
        dia35 = new componentes.DiaCalendario();
        dia36 = new componentes.DiaCalendario();
        dia37 = new componentes.DiaCalendario();
        dia38 = new componentes.DiaCalendario();
        dia39 = new componentes.DiaCalendario();
        dia40 = new componentes.DiaCalendario();
        dia41 = new componentes.DiaCalendario();
        dia42 = new componentes.DiaCalendario();
        lblDiaDestaque = new javax.swing.JLabel();
        panelDiaDestaque = new javax.swing.JPanel();
        panelEventoDestaque = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblCategoriaListagemEvento = new javax.swing.JLabel();
        lblHoraInicioListagemEvento = new javax.swing.JLabel();
        lblHoraFinalListagemEvento = new javax.swing.JLabel();
        importanciaListagemEvento = new componentes.ImportanciaFixo();
        btnEditarEvento = new componentes.Botao1();
        btnExcluirEvento = new componentes.Botao1();
        lblDescricaoEvento = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btnDataAtual = new javax.swing.JButton();
        lblDataMarcada = new javax.swing.JLabel();
        spinAno = new javax.swing.JSpinner();
        visorAno = new javax.swing.JLabel();
        comboMes = new javax.swing.JComboBox<>();
        visorMes = new javax.swing.JLabel();
        btnMesPosterior = new javax.swing.JButton();
        btnMesAnterior = new javax.swing.JButton();
        panelDiasSemana = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        Home = new javax.swing.JPanel();
        MenuCategorias = new javax.swing.JPanel();
        btnCadastrarCategoria = new componentes.Botao1();
        btnCadastrarCategoria1 = new componentes.Botao1();
        botao11 = new componentes.Botao1();
        botao12 = new componentes.Botao1();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        BarraFeramentas.setBackground(new java.awt.Color(25, 52, 65));

        btnPerfil.setFont(new java.awt.Font("Open Sans", 1, 22)); // NOI18N
        btnPerfil.setForeground(new java.awt.Color(252, 255, 245));
        btnPerfil.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnPerfil.setText("<Nome>");
        btnPerfil.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPerfilMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPerfilMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnPerfilMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnPerfilMouseReleased(evt);
            }
        });

        btnMenuCategorias.setText("Categorias");
        btnMenuCategorias.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        btnMenuCategorias.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnMenuCategoriasMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnMenuCategoriasMouseReleased(evt);
            }
        });

        btnMenuEventos.setText("Eventos");
        btnMenuEventos.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        btnMenuEventos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnMenuEventosMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnMenuEventosMouseReleased(evt);
            }
        });

        btnMenuGrupos.setText("Grupos");
        btnMenuGrupos.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        btnMenuGrupos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnMenuGruposMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnMenuGruposMouseReleased(evt);
            }
        });

        btnMenuCalendario.setText("Calendário");
        btnMenuCalendario.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        btnMenuCalendario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnMenuCalendarioMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnMenuCalendarioMouseReleased(evt);
            }
        });

        btnHome.setText("Home");
        btnHome.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        btnHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnHomeMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnHomeMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout BarraFeramentasLayout = new javax.swing.GroupLayout(BarraFeramentas);
        BarraFeramentas.setLayout(BarraFeramentasLayout);
        BarraFeramentasLayout.setHorizontalGroup(
            BarraFeramentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BarraFeramentasLayout.createSequentialGroup()
                .addComponent(btnPerfil, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnHome, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnMenuCategorias, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnMenuEventos, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnMenuGrupos, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnMenuCalendario, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        BarraFeramentasLayout.setVerticalGroup(
            BarraFeramentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnMenuCategorias, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnHome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnMenuEventos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(BarraFeramentasLayout.createSequentialGroup()
                .addGroup(BarraFeramentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnPerfil, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnMenuGrupos, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(btnMenuCalendario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        CardMenu.setPreferredSize(new java.awt.Dimension(800, 600));
        CardMenu.setLayout(new java.awt.CardLayout());

        MenuEventos.setBackground(new java.awt.Color(252, 255, 245));

        btnCadastrarEvento.setText("Cadastrar Evento");
        btnCadastrarEvento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarEventoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout MenuEventosLayout = new javax.swing.GroupLayout(MenuEventos);
        MenuEventos.setLayout(MenuEventosLayout);
        MenuEventosLayout.setHorizontalGroup(
            MenuEventosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuEventosLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(btnCadastrarEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(1113, Short.MAX_VALUE))
        );
        MenuEventosLayout.setVerticalGroup(
            MenuEventosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuEventosLayout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(btnCadastrarEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(538, Short.MAX_VALUE))
        );

        CardMenu.add(MenuEventos, "card4");

        javax.swing.GroupLayout MenuGruposLayout = new javax.swing.GroupLayout(MenuGrupos);
        MenuGrupos.setLayout(MenuGruposLayout);
        MenuGruposLayout.setHorizontalGroup(
            MenuGruposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1388, Short.MAX_VALUE)
        );
        MenuGruposLayout.setVerticalGroup(
            MenuGruposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 650, Short.MAX_VALUE)
        );

        CardMenu.add(MenuGrupos, "card5");

        javax.swing.GroupLayout PerfilLayout = new javax.swing.GroupLayout(Perfil);
        Perfil.setLayout(PerfilLayout);
        PerfilLayout.setHorizontalGroup(
            PerfilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1388, Short.MAX_VALUE)
        );
        PerfilLayout.setVerticalGroup(
            PerfilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 650, Short.MAX_VALUE)
        );

        CardMenu.add(Perfil, "card7");

        Calendario.setBackground(new java.awt.Color(252, 255, 245));

        calendario.setLayout(new java.awt.GridLayout(6, 5, 2, 2));

        dia1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia1);

        dia2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia2);

        dia3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia3);
        dia3.getAccessibleContext().setAccessibleParent(Calendario);

        dia4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia4);
        dia4.getAccessibleContext().setAccessibleParent(Calendario);

        dia5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia5);

        dia6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia6);

        dia7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia7);

        dia8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia8);

        dia9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia9);

        dia10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia10);

        dia11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia11);

        dia12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia12);

        dia13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia13);

        dia14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia14);

        dia15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia15);

        dia16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia16);

        dia17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia17);

        dia18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia18);

        dia19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia19);

        dia20.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia20);

        dia21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia21);

        dia22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia22);

        dia23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia23);

        dia24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia24);

        dia25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia25);

        dia26.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia26);

        dia27.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia27);

        dia28.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia28);

        dia29.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia29);

        dia30.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia30);

        dia31.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia31);

        dia32.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia32);

        dia33.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia33);

        dia34.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia34);

        dia35.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        dia35.setOpaque(false);
        calendario.add(dia35);

        dia36.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia36);

        dia37.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia37);

        dia38.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia38);

        dia39.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia39);

        dia40.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia40);

        dia41.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia41);

        dia42.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(25, 52, 65), 2));
        calendario.add(dia42);

        lblDiaDestaque.setBackground(new java.awt.Color(62, 96, 111));
        lblDiaDestaque.setFont(new java.awt.Font("Open Sans", 1, 22)); // NOI18N
        lblDiaDestaque.setForeground(new java.awt.Color(252, 255, 245));
        lblDiaDestaque.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDiaDestaque.setText("10");
        lblDiaDestaque.setOpaque(true);

        panelDiaDestaque.setBackground(new java.awt.Color(209, 219, 189));

        javax.swing.GroupLayout panelDiaDestaqueLayout = new javax.swing.GroupLayout(panelDiaDestaque);
        panelDiaDestaque.setLayout(panelDiaDestaqueLayout);
        panelDiaDestaqueLayout.setHorizontalGroup(
            panelDiaDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelDiaDestaqueLayout.setVerticalGroup(
            panelDiaDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 229, Short.MAX_VALUE)
        );

        panelEventoDestaque.setBackground(new java.awt.Color(209, 219, 189));

        jLabel5.setFont(new java.awt.Font("Open Sans", 0, 20)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Categoria: ");

        jLabel6.setFont(new java.awt.Font("Open Sans", 0, 20)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Hora de Início:");

        jLabel7.setFont(new java.awt.Font("Open Sans", 0, 20)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Hora de Término:");

        jLabel8.setFont(new java.awt.Font("Open Sans", 0, 20)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Importância:");

        lblCategoriaListagemEvento.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        lblCategoriaListagemEvento.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblHoraInicioListagemEvento.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        lblHoraInicioListagemEvento.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblHoraFinalListagemEvento.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        lblHoraFinalListagemEvento.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        importanciaListagemEvento.setOpaque(false);

        btnEditarEvento.setText("Editar");
        btnEditarEvento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarEventoActionPerformed(evt);
            }
        });

        btnExcluirEvento.setText("Excluir");

        javax.swing.GroupLayout panelEventoDestaqueLayout = new javax.swing.GroupLayout(panelEventoDestaque);
        panelEventoDestaque.setLayout(panelEventoDestaqueLayout);
        panelEventoDestaqueLayout.setHorizontalGroup(
            panelEventoDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelEventoDestaqueLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelEventoDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnEditarEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelEventoDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(panelEventoDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelEventoDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblHoraInicioListagemEvento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCategoriaListagemEvento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblHoraFinalListagemEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(importanciaListagemEvento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExcluirEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        panelEventoDestaqueLayout.setVerticalGroup(
            panelEventoDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEventoDestaqueLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(panelEventoDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblCategoriaListagemEvento, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelEventoDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblHoraInicioListagemEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(panelEventoDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblHoraFinalListagemEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelEventoDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8)
                    .addComponent(importanciaListagemEvento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelEventoDestaqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditarEvento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExcluirEvento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblDescricaoEvento.setBackground(new java.awt.Color(62, 96, 111));
        lblDescricaoEvento.setFont(new java.awt.Font("Open Sans", 0, 20)); // NOI18N
        lblDescricaoEvento.setForeground(new java.awt.Color(252, 255, 245));
        lblDescricaoEvento.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDescricaoEvento.setText("Teste");
        lblDescricaoEvento.setOpaque(true);

        jPanel3.setBackground(new java.awt.Color(252, 255, 245));

        btnDataAtual.setFont(new java.awt.Font("Open Sans", 0, 24)); // NOI18N
        btnDataAtual.setText("Hoje");
        btnDataAtual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDataAtualActionPerformed(evt);
            }
        });

        lblDataMarcada.setFont(new java.awt.Font("Open Sans", 0, 36)); // NOI18N
        lblDataMarcada.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDataMarcada.setText("Outubro de 2017");
        lblDataMarcada.setPreferredSize(new java.awt.Dimension(288, 60));

        spinAno.setModel(new javax.swing.SpinnerNumberModel());
        spinAno.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinAnoStateChanged(evt);
            }
        });

        visorAno.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        visorAno.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        visorAno.setText("2017");

        comboMes.setFont(new java.awt.Font("Open Sans", 0, 20)); // NOI18N
        comboMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" }));
        comboMes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboMesActionPerformed(evt);
            }
        });

        visorMes.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        visorMes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        visorMes.setText("Janeiro");

        btnMesPosterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/SetaBaixo.png"))); // NOI18N
        btnMesPosterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMesPosteriorActionPerformed(evt);
            }
        });

        btnMesAnterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/SetaCima.png"))); // NOI18N
        btnMesAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMesAnteriorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(visorMes, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnMesPosterior, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnMesAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboMes, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spinAno, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(visorAno, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(lblDataMarcada, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnDataAtual)
                .addGap(0, 0, 0))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(spinAno, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblDataMarcada, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnDataAtual))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addComponent(visorAno)
                                .addGap(28, 28, 28))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(visorMes)
                        .addGap(0, 0, 0)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(comboMes, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addComponent(btnMesAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(2, 2, 2)
                                .addComponent(btnMesPosterior, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))))))
        );

        panelDiasSemana.setLayout(new java.awt.GridLayout(1, 7, 2, 0));

        jLabel10.setBackground(new java.awt.Color(62, 96, 111));
        jLabel10.setFont(new java.awt.Font("Open Sans", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(252, 255, 245));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Domingo");
        jLabel10.setOpaque(true);
        jLabel10.setPreferredSize(new java.awt.Dimension(108, 152));
        panelDiasSemana.add(jLabel10);

        jLabel11.setBackground(new java.awt.Color(62, 96, 111));
        jLabel11.setFont(new java.awt.Font("Open Sans", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(252, 255, 245));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("Segunda");
        jLabel11.setOpaque(true);
        jLabel11.setPreferredSize(new java.awt.Dimension(108, 152));
        panelDiasSemana.add(jLabel11);

        jLabel12.setBackground(new java.awt.Color(62, 96, 111));
        jLabel12.setFont(new java.awt.Font("Open Sans", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(252, 255, 245));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Terça");
        jLabel12.setOpaque(true);
        jLabel12.setPreferredSize(new java.awt.Dimension(108, 152));
        panelDiasSemana.add(jLabel12);

        jLabel13.setBackground(new java.awt.Color(62, 96, 111));
        jLabel13.setFont(new java.awt.Font("Open Sans", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(252, 255, 245));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Quarta");
        jLabel13.setOpaque(true);
        jLabel13.setPreferredSize(new java.awt.Dimension(108, 152));
        panelDiasSemana.add(jLabel13);

        jLabel14.setBackground(new java.awt.Color(62, 96, 111));
        jLabel14.setFont(new java.awt.Font("Open Sans", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(252, 255, 245));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Quinta");
        jLabel14.setOpaque(true);
        jLabel14.setPreferredSize(new java.awt.Dimension(108, 152));
        panelDiasSemana.add(jLabel14);

        jLabel15.setBackground(new java.awt.Color(62, 96, 111));
        jLabel15.setFont(new java.awt.Font("Open Sans", 1, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(252, 255, 245));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("Sexta");
        jLabel15.setOpaque(true);
        jLabel15.setPreferredSize(new java.awt.Dimension(108, 152));
        panelDiasSemana.add(jLabel15);

        jLabel16.setBackground(new java.awt.Color(62, 96, 111));
        jLabel16.setFont(new java.awt.Font("Open Sans", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(252, 255, 245));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("Sábado");
        jLabel16.setOpaque(true);
        jLabel16.setPreferredSize(new java.awt.Dimension(108, 152));
        panelDiasSemana.add(jLabel16);

        javax.swing.GroupLayout CalendarioLayout = new javax.swing.GroupLayout(Calendario);
        Calendario.setLayout(CalendarioLayout);
        CalendarioLayout.setHorizontalGroup(
            CalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CalendarioLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(CalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelDiasSemana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(CalendarioLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(calendario, javax.swing.GroupLayout.PREFERRED_SIZE, 900, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(CalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelDiaDestaque, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblDescricaoEvento, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelEventoDestaque, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblDiaDestaque, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(60, 60, 60))
            .addGroup(CalendarioLayout.createSequentialGroup()
                .addGap(285, 285, 285)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        CalendarioLayout.setVerticalGroup(
            CalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CalendarioLayout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(CalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CalendarioLayout.createSequentialGroup()
                        .addComponent(lblDiaDestaque, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(panelDiaDestaque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblDescricaoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(panelEventoDestaque, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(CalendarioLayout.createSequentialGroup()
                        .addComponent(panelDiasSemana, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(calendario, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        CardMenu.add(Calendario, "card6");

        javax.swing.GroupLayout HomeLayout = new javax.swing.GroupLayout(Home);
        Home.setLayout(HomeLayout);
        HomeLayout.setHorizontalGroup(
            HomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1388, Short.MAX_VALUE)
        );
        HomeLayout.setVerticalGroup(
            HomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 650, Short.MAX_VALUE)
        );

        CardMenu.add(Home, "card2");

        MenuCategorias.setBackground(new java.awt.Color(252, 255, 245));

        btnCadastrarCategoria.setText("Pesquisar Categoria");
        btnCadastrarCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarCategoriaActionPerformed(evt);
            }
        });

        btnCadastrarCategoria1.setText("Cadastrar Categoria");
        btnCadastrarCategoria1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarCategoria1ActionPerformed(evt);
            }
        });

        botao11.setText("Editar Categorias");
        botao11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botao11ActionPerformed(evt);
            }
        });

        botao12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botao12ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Open Sans", 0, 22)); // NOI18N
        jLabel1.setText("jLabel1");

        javax.swing.GroupLayout MenuCategoriasLayout = new javax.swing.GroupLayout(MenuCategorias);
        MenuCategorias.setLayout(MenuCategoriasLayout);
        MenuCategoriasLayout.setHorizontalGroup(
            MenuCategoriasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuCategoriasLayout.createSequentialGroup()
                .addGroup(MenuCategoriasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MenuCategoriasLayout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addGroup(MenuCategoriasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botao11, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCadastrarCategoria1, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(135, 135, 135)
                        .addGroup(MenuCategoriasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnCadastrarCategoria, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                            .addComponent(botao12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(MenuCategoriasLayout.createSequentialGroup()
                        .addGap(322, 322, 322)
                        .addComponent(jLabel1)))
                .addContainerGap(745, Short.MAX_VALUE))
        );
        MenuCategoriasLayout.setVerticalGroup(
            MenuCategoriasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuCategoriasLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 266, Short.MAX_VALUE)
                .addGroup(MenuCategoriasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCadastrarCategoria1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCadastrarCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(98, 98, 98)
                .addGroup(MenuCategoriasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botao11, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botao12, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(114, 114, 114))
        );

        CardMenu.add(MenuCategorias, "card3");

        javax.swing.GroupLayout MenuPrincipalLayout = new javax.swing.GroupLayout(MenuPrincipal);
        MenuPrincipal.setLayout(MenuPrincipalLayout);
        MenuPrincipalLayout.setHorizontalGroup(
            MenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(MenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(MenuPrincipalLayout.createSequentialGroup()
                    .addComponent(BarraFeramentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(MenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(MenuPrincipalLayout.createSequentialGroup()
                    .addComponent(CardMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 1360, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        MenuPrincipalLayout.setVerticalGroup(
            MenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 700, Short.MAX_VALUE)
            .addGroup(MenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(MenuPrincipalLayout.createSequentialGroup()
                    .addComponent(BarraFeramentas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 650, Short.MAX_VALUE)))
            .addGroup(MenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MenuPrincipalLayout.createSequentialGroup()
                    .addGap(0, 50, Short.MAX_VALUE)
                    .addComponent(CardMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 650, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MenuPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 1360, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(MenuPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    boolean btnPerfilPressed;
    
    private void btnPerfilMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPerfilMouseEntered
        this.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_btnPerfilMouseEntered

    private void btnPerfilMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPerfilMouseExited
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        btnPerfilPressed = false;
    }//GEN-LAST:event_btnPerfilMouseExited

    private void btnCadastrarCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarCategoriaActionPerformed
        CadastroCategoria cadastro = new CadastroCategoria();
        cadastro.setResizable(false);
        cadastro.setVisible(true);
    }//GEN-LAST:event_btnCadastrarCategoriaActionPerformed

    private void btnCadastrarEventoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarEventoActionPerformed
        CadastroEvento cadastro = new CadastroEvento();
        cadastro.abrirCadastro(usuario);
    }//GEN-LAST:event_btnCadastrarEventoActionPerformed

    private void btnCadastrarCategoria1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarCategoria1ActionPerformed
        CadastroCategoria cadastro = new CadastroCategoria();
        cadastro.abrirCadastro(usuario);
    }//GEN-LAST:event_btnCadastrarCategoria1ActionPerformed

    private void botao11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botao11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_botao11ActionPerformed

    private void botao12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botao12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_botao12ActionPerformed

    private void comboMesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboMesActionPerformed
        this.mes = comboMes.getSelectedIndex()+1;
        lblDiaDestaque.setText("");
        panelDiaDestaque.removeAll();
        atualizarCalendario();
    }//GEN-LAST:event_comboMesActionPerformed

    private void btnDataAtualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDataAtualActionPerformed
        limparDadosEvento();
        inicializarCalendario();
    }//GEN-LAST:event_btnDataAtualActionPerformed

    private void btnMesAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMesAnteriorActionPerformed
        if(comboMes.getSelectedIndex() != 0){
            comboMes.setSelectedIndex(comboMes.getSelectedIndex()-1);
            lblDiaDestaque.setText("");
            panelDiaDestaque.removeAll();
            limparDadosEvento();
            atualizarCalendario();
        }
    }//GEN-LAST:event_btnMesAnteriorActionPerformed

    private void btnMesPosteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMesPosteriorActionPerformed
        if(comboMes.getSelectedIndex() != 11){
            comboMes.setSelectedIndex(comboMes.getSelectedIndex()+1);
            lblDiaDestaque.setText("");
            panelDiaDestaque.removeAll();
            limparDadosEvento();
            atualizarCalendario();
        }
    }//GEN-LAST:event_btnMesPosteriorActionPerformed

    private void spinAnoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinAnoStateChanged
        this.ano = (int) spinAno.getValue();
        lblDiaDestaque.setText("");
        panelDiaDestaque.removeAll();
        limparDadosEvento();
        atualizarCalendario();
    }//GEN-LAST:event_spinAnoStateChanged

    private void btnPerfilMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPerfilMousePressed
        btnPerfilPressed = true;
    }//GEN-LAST:event_btnPerfilMousePressed

    private void btnPerfilMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPerfilMouseReleased
        if(btnPerfilPressed){
            CardLayout card = (CardLayout)CardMenu.getLayout();
            card.show(CardMenu,"card7");
            switchButtons(false, false, false, false, false);
        }
    }//GEN-LAST:event_btnPerfilMouseReleased

    private void btnHomeMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHomeMousePressed
        btnHome.setPressed(true);
    }//GEN-LAST:event_btnHomeMousePressed

    private void btnHomeMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHomeMouseReleased
        if(btnHome.getPressed()){
            CardLayout card = (CardLayout)CardMenu.getLayout();
            card.show(CardMenu,"card2");
            switchButtons(true, false, false, false, false);
        }
    }//GEN-LAST:event_btnHomeMouseReleased

    private void btnMenuCategoriasMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMenuCategoriasMousePressed
        btnMenuCategorias.setPressed(true);
    }//GEN-LAST:event_btnMenuCategoriasMousePressed

    private void btnMenuCategoriasMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMenuCategoriasMouseReleased
        if(btnMenuCategorias.getPressed()){
            CardLayout card = (CardLayout)CardMenu.getLayout();
            card.show(CardMenu,"card3");
            switchButtons(false, true, false, false, false);
        }
    }//GEN-LAST:event_btnMenuCategoriasMouseReleased

    private void btnMenuEventosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMenuEventosMousePressed
        btnMenuEventos.setPressed(true);
    }//GEN-LAST:event_btnMenuEventosMousePressed

    private void btnMenuEventosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMenuEventosMouseReleased
        if(btnMenuEventos.getPressed()){
            CardLayout card = (CardLayout)CardMenu.getLayout();
            card.show(CardMenu,"card4");
            switchButtons(false, false, true, false, false);
        }
    }//GEN-LAST:event_btnMenuEventosMouseReleased

    private void btnMenuGruposMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMenuGruposMousePressed
        btnMenuGrupos.setPressed(true);
    }//GEN-LAST:event_btnMenuGruposMousePressed

    private void btnMenuGruposMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMenuGruposMouseReleased
        if(btnMenuGrupos.getPressed()){
            CardLayout card = (CardLayout)CardMenu.getLayout();
            card.show(CardMenu,"card5");
            switchButtons(false, false, false, true, false);
        }
    }//GEN-LAST:event_btnMenuGruposMouseReleased

    private void btnMenuCalendarioMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMenuCalendarioMousePressed
        btnMenuCalendario.setPressed(true);
    }//GEN-LAST:event_btnMenuCalendarioMousePressed

    private void btnMenuCalendarioMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMenuCalendarioMouseReleased
        if(btnMenuCalendario.getPressed()){
            CardLayout card = (CardLayout)CardMenu.getLayout();
            card.show(CardMenu,"card6");
            switchButtons(false, false, false, false, true);
            atualizarCalendario();
        }
    }//GEN-LAST:event_btnMenuCalendarioMouseReleased

    private void btnEditarEventoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarEventoActionPerformed
        EditarEvento edicao = new EditarEvento();
        edicao.abrirEdicaoEvento(this.eventoSelecionado, this, usuario);
    }//GEN-LAST:event_btnEditarEventoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BarraFeramentas;
    private javax.swing.JPanel Calendario;
    private javax.swing.JPanel CardMenu;
    private javax.swing.JPanel Home;
    private javax.swing.JPanel MenuCategorias;
    private javax.swing.JPanel MenuEventos;
    private javax.swing.JPanel MenuGrupos;
    private javax.swing.JPanel MenuPrincipal;
    private javax.swing.JPanel Perfil;
    private componentes.Botao1 botao11;
    private componentes.Botao1 botao12;
    private componentes.Botao1 btnCadastrarCategoria;
    private componentes.Botao1 btnCadastrarCategoria1;
    private componentes.Botao1 btnCadastrarEvento;
    private javax.swing.JButton btnDataAtual;
    private componentes.Botao1 btnEditarEvento;
    private componentes.Botao1 btnExcluirEvento;
    private componentes.Botao2 btnHome;
    private componentes.Botao2 btnMenuCalendario;
    private componentes.Botao2 btnMenuCategorias;
    private componentes.Botao2 btnMenuEventos;
    private componentes.Botao2 btnMenuGrupos;
    private javax.swing.JButton btnMesAnterior;
    private javax.swing.JButton btnMesPosterior;
    private javax.swing.JLabel btnPerfil;
    private javax.swing.JPanel calendario;
    private javax.swing.JComboBox<String> comboMes;
    private componentes.DiaCalendario dia1;
    private componentes.DiaCalendario dia10;
    private componentes.DiaCalendario dia11;
    private componentes.DiaCalendario dia12;
    private componentes.DiaCalendario dia13;
    private componentes.DiaCalendario dia14;
    private componentes.DiaCalendario dia15;
    private componentes.DiaCalendario dia16;
    private componentes.DiaCalendario dia17;
    private componentes.DiaCalendario dia18;
    private componentes.DiaCalendario dia19;
    private componentes.DiaCalendario dia2;
    private componentes.DiaCalendario dia20;
    private componentes.DiaCalendario dia21;
    private componentes.DiaCalendario dia22;
    private componentes.DiaCalendario dia23;
    private componentes.DiaCalendario dia24;
    private componentes.DiaCalendario dia25;
    private componentes.DiaCalendario dia26;
    private componentes.DiaCalendario dia27;
    private componentes.DiaCalendario dia28;
    private componentes.DiaCalendario dia29;
    private componentes.DiaCalendario dia3;
    private componentes.DiaCalendario dia30;
    private componentes.DiaCalendario dia31;
    private componentes.DiaCalendario dia32;
    private componentes.DiaCalendario dia33;
    private componentes.DiaCalendario dia34;
    private componentes.DiaCalendario dia35;
    private componentes.DiaCalendario dia36;
    private componentes.DiaCalendario dia37;
    private componentes.DiaCalendario dia38;
    private componentes.DiaCalendario dia39;
    private componentes.DiaCalendario dia4;
    private componentes.DiaCalendario dia40;
    private componentes.DiaCalendario dia41;
    private componentes.DiaCalendario dia42;
    private componentes.DiaCalendario dia5;
    private componentes.DiaCalendario dia6;
    private componentes.DiaCalendario dia7;
    private componentes.DiaCalendario dia8;
    private componentes.DiaCalendario dia9;
    private componentes.ImportanciaFixo importanciaListagemEvento;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblCategoriaListagemEvento;
    private javax.swing.JLabel lblDataMarcada;
    private javax.swing.JLabel lblDescricaoEvento;
    private javax.swing.JLabel lblDiaDestaque;
    private javax.swing.JLabel lblHoraFinalListagemEvento;
    private javax.swing.JLabel lblHoraInicioListagemEvento;
    private javax.swing.JPanel panelDiaDestaque;
    private javax.swing.JPanel panelDiasSemana;
    private javax.swing.JPanel panelEventoDestaque;
    private javax.swing.JSpinner spinAno;
    private javax.swing.JLabel visorAno;
    private javax.swing.JLabel visorMes;
    // End of variables declaration//GEN-END:variables
}
